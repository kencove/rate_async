import aiohttp
import asyncio
import time
import asyncpg
# import datetime
import random

start_time = time.time()
db_url = 'postgresql://postgres@localhost/test'


async def get_pokemon(session, url, http_id):
    async with session.get(url) as resp:
        conn = await asyncpg.connect(db_url)
        await conn.execute('''
            UPDATE http_request set process_start = now() where id = $1
        ''', http_id)
        pokemon = await resp.json()
        rnd = random.randint(1, 10)
        time.sleep(rnd)
        await conn.execute('''
            UPDATE http_request set process_end = now() where id = $1
        ''', http_id)
        print("--- %s seconds for %s ---" % (rnd, pokemon['name']))
        return pokemon['name']


async def main():
    conn = await asyncpg.connect(db_url)
    await conn.execute('''
        CREATE TABLE if not exists public.http_request
        (
            id               serial
                primary key,
            uri              text,
            method           varchar(10),
            body             text,
            headers          text,
            response_body    text,
            response_status  varchar(10),
            response_headers text,
            processcount     text,
            reference        varchar,
            reference_type   varchar,
            created_date     timestamp,
            complete_date    timestamp,
            process_user     varchar(255),
            process_id       varchar(255),
            process_start    timestamp,
            process_end      timestamp
        );
    ''')

    async with aiohttp.ClientSession() as session:
        tasks = []
        for number in range(1, 10):
            url = f'https://pokeapi.co/api/v2/pokemon/{number}'
            results = await conn.fetchrow('''
                INSERT INTO http_request(uri,created_date) 
                VALUES($1,now()) 
                RETURNING id
            ''', url)
            tasks.append(asyncio.ensure_future(get_pokemon(
                session, url, results['id'])))

        original_pokemon = await asyncio.gather(*tasks)
        for pokemon in original_pokemon:
            print(pokemon)

    await conn.close()

asyncio.run(main())
print("--- %s seconds ---" % (time.time() - start_time))
