# test async

## install

dependency postgres ( brew local or docker )

```sql
brew install postgresql@15       
brew services start postgresql@15
su postgres && psql -c "create database test"
```

```bash
poetry install
```
debug file ( make sure you have the right interpreter )

## results

```sql
select id, uri
  , created_date, process_start, process_end
  , process_end - created_date job_tm
  , process_end - process_start pr_tm
from http_request r
order by r.id desc;
```

```
id,uri,created_date,process_start,process_end,job_tm,pr_tm
1071,https://pokeapi.co/api/v2/pokemon/9,2023-11-18 19:27:26.021157,2023-11-18 19:27:41.196907,2023-11-18 19:28:06.229070,0 years 0 mons 0 days 0 hours 0 mins 40.207913 secs,0 years 0 mons 0 days 0 hours 0 mins 25.032163 secs
1070,https://pokeapi.co/api/v2/pokemon/8,2023-11-18 19:27:26.020809,2023-11-18 19:27:41.197536,2023-11-18 19:28:12.265235,0 years 0 mons 0 days 0 hours 0 mins 46.244426 secs,0 years 0 mons 0 days 0 hours 0 mins 31.067699 secs
1069,https://pokeapi.co/api/v2/pokemon/7,2023-11-18 19:27:26.020459,2023-11-18 19:28:12.266362,2023-11-18 19:28:14.314905,0 years 0 mons 0 days 0 hours 0 mins 48.294446 secs,0 years 0 mons 0 days 0 hours 0 mins 2.048543 secs
1068,https://pokeapi.co/api/v2/pokemon/6,2023-11-18 19:27:26.019356,2023-11-18 19:27:26.101244,2023-11-18 19:27:31.138208,0 years 0 mons 0 days 0 hours 0 mins 5.118852 secs,0 years 0 mons 0 days 0 hours 0 mins 5.036964 secs
1067,https://pokeapi.co/api/v2/pokemon/5,2023-11-18 19:27:26.018873,2023-11-18 19:27:41.195999,2023-11-18 19:27:46.210513,0 years 0 mons 0 days 0 hours 0 mins 20.19164 secs,0 years 0 mons 0 days 0 hours 0 mins 5.014514 secs
1066,https://pokeapi.co/api/v2/pokemon/4,2023-11-18 19:27:26.018541,2023-11-18 19:27:26.102824,2023-11-18 19:27:41.145108,0 years 0 mons 0 days 0 hours 0 mins 15.126567 secs,0 years 0 mons 0 days 0 hours 0 mins 15.042284 secs
1065,https://pokeapi.co/api/v2/pokemon/3,2023-11-18 19:27:26.017944,2023-11-18 19:27:41.197462,2023-11-18 19:28:07.241306,0 years 0 mons 0 days 0 hours 0 mins 41.223362 secs,0 years 0 mons 0 days 0 hours 0 mins 26.043844 secs
1064,https://pokeapi.co/api/v2/pokemon/2,2023-11-18 19:27:26.006601,2023-11-18 19:27:41.174074,2023-11-18 19:27:56.222081,0 years 0 mons 0 days 0 hours 0 mins 30.21548 secs,0 years 0 mons 0 days 0 hours 0 mins 15.048007 secs
1063,https://pokeapi.co/api/v2/pokemon/1,2023-11-18 19:27:26.004195,2023-11-18 19:27:41.197548,2023-11-18 19:28:10.250269,0 years 0 mons 0 days 0 hours 0 mins 44.246074 secs,0 years 0 mons 0 days 0 hours 0 mins 29.052721 secs
```


```
git remote add origin git@gitlab.com:kencove/rate_async.git
git push --set-upstream origin --all
git push --set-upstream origin --tags
```
